'From Cuis 5.0 of 7 November 2016 [latest update: #3068] on 14 June 2017 at 7:35:41 pm'!
'Description Please enter a description for this package'!
!provides: 'BrowserNavigation' 1 11!
!requires: 'Morphic-Widgets-Extras' 1 11 nil!
!classDefinition: #BrowserNav category: #BrowserNavigation!
Browser subclass: #BrowserNav
	instanceVariableNames: 'locationIndex navigationChain registerBrowse'
	classVariableNames: 'BrowsingAccess BrowsingHistory BrowsingUpdates'
	poolDictionaries: ''
	category: 'BrowserNavigation'!
!classDefinition: 'BrowserNav class' category: #BrowserNavigation!
BrowserNav class
	instanceVariableNames: ''!

!classDefinition: #CountingCollection category: #BrowserNavigation!
SortedCollection subclass: #CountingCollection
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'BrowserNavigation'!
!classDefinition: 'CountingCollection class' category: #BrowserNavigation!
CountingCollection class
	instanceVariableNames: ''!

!classDefinition: #BrowserWindowNav category: #BrowserNavigation!
BrowserWindow subclass: #BrowserWindowNav
	instanceVariableNames: 'buttons aproposInput'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'BrowserNavigation'!
!classDefinition: 'BrowserWindowNav class' category: #BrowserNavigation!
BrowserWindowNav class
	instanceVariableNames: ''!


!BrowserNav methodsFor: 'navigation' stamp: 'MarianoMontone 6/14/2017 10:37:40'!
browseTo: aLocation
	
	|class|
	
	class _ Smalltalk classNamed: aLocation first.
	
	self setClass: class selector: aLocation second! !

!BrowserNav methodsFor: 'browsing' stamp: 'MarianoMontone 6/14/2017 18:51:12'!
browsingAccess
	^ self class browsingAccess! !

!BrowserNav methodsFor: 'browsing' stamp: 'MarianoMontone 6/13/2017 21:40:14'!
browsingHistory
	^ self class browsingHistory! !

!BrowserNav methodsFor: 'browsing' stamp: 'MarianoMontone 6/14/2017 09:50:54'!
browsingHistoryMaxSize
	^ self class browsingHistoryMaxSize! !

!BrowserNav methodsFor: 'browsing' stamp: 'MarianoMontone 6/14/2017 19:14:31'!
browsingUpdates
	^ self class browsingUpdates! !

!BrowserNav methodsFor: 'as yet unclassified' stamp: 'MarianoMontone 6/14/2017 19:24:11'!
defineMessageFrom: aString notifying: aRequestor
	|selector|
		
	selector := super defineMessageFrom: aString notifying: aRequestor.
	
	selector ifNotNil: [ |location|
		location _ {self selectedClassOrMetaClass . selector}.
		self browsingUpdates addFirst: location].
	
	^ selector! !

!BrowserNav methodsFor: 'as yet unclassified' stamp: 'MarianoMontone 6/14/2017 10:31:41'!
initialize
	super initialize.
	
	locationIndex := nil.
	navigationChain := OrderedCollection new.
	registerBrowse := true! !

!BrowserNav methodsFor: 'browsing' stamp: 'MarianoMontone 6/14/2017 10:33:03'!
messageListIndex: anInteger
	super messageListIndex: anInteger.
	
	registerBrowse ifTrue: [
	
		"Add to browsing history"
		self registerBrowsingHistory.
	
		"Add to navigation chain"
		self registerNavigation] ! !

!BrowserNav methodsFor: 'navigation' stamp: 'MarianoMontone 6/13/2017 23:44:30'!
navigateBack
	
	locationIndex ifNil: [^ nil].
	(locationIndex >= self navigationChain size) ifTrue: [^nil].
	
	locationIndex := locationIndex + 1.
	
	self navigateTo: (self navigationChain at: locationIndex)! !

!BrowserNav methodsFor: 'navigation' stamp: 'MarianoMontone 6/13/2017 22:06:02'!
navigateForward
	
	locationIndex ifNil: [^ nil].
	(locationIndex <= 1) ifTrue: [^nil].
	
	locationIndex := locationIndex - 1.
	
	self navigateTo: (self navigationChain at: locationIndex)! !

!BrowserNav methodsFor: 'navigation' stamp: 'MarianoMontone 6/14/2017 10:30:13'!
navigateTo: aLocation
	
	|class|
	
	class _ Smalltalk classNamed: aLocation first.
	
	self withRegisterDisabled: [
		self setClass: class selector: aLocation second]! !

!BrowserNav methodsFor: 'navigation' stamp: 'MarianoMontone 6/13/2017 22:48:16'!
navigationChain
	^ navigationChain! !

!BrowserNav methodsFor: 'browsing' stamp: 'MarianoMontone 6/14/2017 19:14:40'!
registerBrowsingHistory
	
	|browsingHistory browsingAccess |
	
	browsingHistory _ self browsingHistory.
	browsingAccess _ self browsingAccess.

	selectedMessage ifNotNil: [ |location|
		
		location _ {selectedClassName. selectedMessage}.
		
		browsingAccess add: location.	

		(browsingHistory isEmpty not and: [location = browsingHistory first]) ifFalse: [
			browsingHistory addFirst: location.
	
			(browsingHistory size > self browsingHistoryMaxSize) ifTrue: [
				browsingHistory removeLast.	
			]]] ! !

!BrowserNav methodsFor: 'navigation' stamp: 'MarianoMontone 6/14/2017 10:34:56'!
registerNavigation

	|location|
	
	selectedMessage ifNotNil: [
	
		(locationIndex notNil and: [locationIndex > 1]) ifTrue: [
			navigationChain := navigationChain copyFrom: locationIndex to: navigationChain size].
	
		location  := {selectedClassName. selectedMessage}.
		
		navigationChain addFirst: location.
		locationIndex := 1
		]! !

!BrowserNav methodsFor: 'as yet unclassified' stamp: 'MarianoMontone 6/14/2017 19:14:11'!
withRegisterDisabled: aBlock

	|regBrowse |
	
	regBrowse _ registerBrowse.
	registerBrowse := false.
	aBlock value.
	registerBrowse := regBrowse ! !

!BrowserNav class methodsFor: 'as yet unclassified' stamp: 'MarianoMontone 6/14/2017 18:51:22'!
browsingAccess
	BrowsingAccess ifNil: [
		BrowsingAccess := CountingCollection new].
	^BrowsingAccess! !

!BrowserNav class methodsFor: 'as yet unclassified' stamp: 'MarianoMontone 6/13/2017 21:39:35'!
browsingHistory
	BrowsingHistory ifNil: [
		BrowsingHistory := OrderedCollection new].
	^BrowsingHistory! !

!BrowserNav class methodsFor: 'as yet unclassified' stamp: 'MarianoMontone 6/14/2017 09:52:54'!
browsingHistoryMaxSize
	^ 100! !

!BrowserNav class methodsFor: 'as yet unclassified' stamp: 'MarianoMontone 6/14/2017 18:52:56'!
browsingUpdates
	BrowsingUpdates ifNil: [
		BrowsingUpdates := OrderedCollection new].
	^BrowsingUpdates! !

!BrowserNav class methodsFor: 'as yet unclassified' stamp: 'MarianoMontone 6/14/2017 18:51:48'!
clearBrowsingAccess
	BrowsingAccess := CountingCollection new! !

!BrowserNav class methodsFor: 'as yet unclassified' stamp: 'MarianoMontone 6/14/2017 09:48:01'!
clearBrowsingHistory
	BrowsingHistory := OrderedCollection new! !

!BrowserNav class methodsFor: 'as yet unclassified' stamp: 'MarianoMontone 6/14/2017 18:53:16'!
clearBrowsingUpdates
	BrowsingUpdates := OrderedCollection new! !

!CountingCollection methodsFor: 'as yet unclassified' stamp: 'MarianoMontone 6/14/2017 18:46:59'!
add: anObject
	^ self add: anObject count: 1! !

!CountingCollection methodsFor: 'as yet unclassified' stamp: 'MarianoMontone 6/14/2017 18:46:48'!
add: anObject count: aNumber

	|x|
	x := self detect: [:elem  | elem first = anObject] ifNone: [].
	
	x ifNotNil:[
		x at: 1 put: x first + aNumber.
		self reSort]
	ifNil:[
		super add: {aNumber. anObject}]! !

!CountingCollection methodsFor: 'as yet unclassified' stamp: 'MarianoMontone 6/14/2017 18:45:29'!
detectCounting: aBlock ifNone: exceptionBlock 
	"Evaluate aBlock with each of the receiver's elements as the argument. 
	Answer the first element for which aBlock evaluates to true. If none 
	evaluate to true, then evaluate the argument, exceptionBlock."

	super do: [:each  | (aBlock value: each) ifTrue: [^each]].
	^exceptionBlock value! !

!CountingCollection methodsFor: 'as yet unclassified' stamp: 'MarianoMontone 6/14/2017 18:45:17'!
doCounting: aBlock
	^ super do: [:x | aBlock value: x second value: x first] ! !

!CountingCollection methodsFor: 'initialization' stamp: 'MarianoMontone 6/14/2017 18:36:39'!
initialize
	super initialize.
	sortBlock _ [:x :y | x first > y first]! !

!BrowserWindowNav methodsFor: 'as yet unclassified' stamp: 'MarianoMontone 6/14/2017 13:13:36'!
apropos

	|text|
	
	text _ aproposInput contents.
	
	(text first isUppercase ) ifTrue: [
		"Apropos a class"
		self aproposClass: text	
	] ifFalse: [
		self aproposMethod: text].
	
	aproposInput contents: ''.! !

!BrowserWindowNav methodsFor: 'as yet unclassified' stamp: 'MarianoMontone 6/14/2017 13:12:12'!
aproposClass: pattern

	"Search for a class by name."
	| foundClass classNames index toMatch exactMatch potentialClassNames |

	self okToChange ifFalse: [ ^self flash ].
	pattern isEmpty ifTrue: [^ self flash].
	toMatch _ (pattern copyWithout: $.) asLowercase withBlanksTrimmed.
	potentialClassNames _ model potentialClassNames asOrderedCollection.
	classNames _ (pattern last = $. or: [pattern last = $ ])
		ifTrue: [potentialClassNames select:
					[:nm |  nm asLowercase = toMatch]]
		ifFalse: [potentialClassNames select: 
					[:n | n includesSubstring: toMatch caseSensitive: false]].
	classNames isEmpty ifTrue: [^ self flash].
	exactMatch _ classNames detect: [ :each | each asLowercase = toMatch] ifNone: nil.

	index _ classNames size = 1
		ifTrue:	[1]
		ifFalse:	[exactMatch
			ifNil: [(PopUpMenu labelArray: classNames lines: #()) startUpMenu]
			ifNotNil: [classNames addFirst: exactMatch.
				(PopUpMenu labelArray: classNames lines: #(1)) startUpMenu]].
	index = 0 ifTrue: [^ self flash].
	foundClass _ Smalltalk at: (classNames at: index) asSymbol.
 	model selectCategoryForClass: foundClass.
	model selectClass: foundClass
! !

!BrowserWindowNav methodsFor: 'browsing' stamp: 'MarianoMontone 6/14/2017 19:27:26'!
browsingAccess
	^ model browsingAccess! !

!BrowserWindowNav methodsFor: 'browsing' stamp: 'MarianoMontone 6/13/2017 21:58:01'!
browsingHistory
	^ model browsingHistory! !

!BrowserWindowNav methodsFor: 'browsing' stamp: 'MarianoMontone 6/14/2017 19:27:42'!
browsingUpdates
	^ model browsingUpdates! !

!BrowserWindowNav methodsFor: 'GUI building' stamp: 'MarianoMontone 6/13/2017 23:25:34'!
createBackBox
	^(PluggableButtonMorph model: self action: #navigateBack)
		icon: (Theme current fetch: #('16x16' 'actions' 'go-previous'));
		iconName: #navigateBack;
		setBalloonText: 'navigate back';
		morphExtent: self boxExtent! !

!BrowserWindowNav methodsFor: 'GUI building' stamp: 'MarianoMontone 6/14/2017 12:30:08'!
createBrowsingAccessBox
	^(PluggableButtonMorph model: self action: #showBrowsingAccess)
		icon: (Theme current fetch: #('16x16' 'actions' 'appointment-new'));
		iconName: #browsingAccess;
		setBalloonText: 'show browsing access';
		morphExtent: self boxExtent! !

!BrowserWindowNav methodsFor: 'GUI building' stamp: 'MarianoMontone 6/14/2017 12:30:16'!
createBrowsingHistoryBox
	^(PluggableButtonMorph model: self action: #showBrowsingHistory)
		icon: (Theme current fetch: #('16x16' 'actions' 'address-book-new'));
		iconName: #browsingHistory;
		setBalloonText: 'show browsing history';
		morphExtent: self boxExtent! !

!BrowserWindowNav methodsFor: 'GUI building' stamp: 'MarianoMontone 6/13/2017 23:28:47'!
createForwardBox
	^(PluggableButtonMorph model: self action: #navigateForward)
		icon: (Theme current fetch: #('16x16' 'actions' 'go-next'));
		iconName: #navigateForward;
		setBalloonText: 'navigate forward';
		morphExtent: self boxExtent! !

!BrowserWindowNav methodsFor: 'initialization' stamp: 'MarianoMontone 6/14/2017 09:29:57'!
initialize
	super initialize.
	
	self initializeNavigationArea.! !

!BrowserWindowNav methodsFor: 'initialization' stamp: 'MarianoMontone 6/14/2017 13:01:46'!
initializeNavigationArea

	buttons := LayoutMorph newRow
					padding: #right;
					separation: 4;
					morphExtent: 200@20;
					yourself.
					
	aproposInput _ (OneLineEditorMorph contents: '')
							color: Color white;
							morphWidth: 50;
							crAction:[self apropos];
							yourself.
	
	buttons addMorph: aproposInput.
	buttons addMorph: self createBrowsingAccessBox .
	buttons addMorph: self createBrowsingHistoryBox.
	buttons addMorph: self createBackBox.
	buttons addMorph: self createForwardBox.
	
	self addMorph: buttons position: 0@0! !

!BrowserWindowNav methodsFor: 'GUI building' stamp: 'MarianoMontone 6/14/2017 12:58:34'!
layoutSubmorphs
	
	
	buttons morphHeight: 30.
	
	"buttons morphPosition: (self morphExtent x - buttons morphWidth)@0."
	
	buttons morphPosition: (self morphExtent x - 200)@0.	
	
	super layoutSubmorphs.! !

!BrowserWindowNav methodsFor: 'navigation' stamp: 'MarianoMontone 6/13/2017 23:25:54'!
navigateBack
	model navigateBack! !

!BrowserWindowNav methodsFor: 'navigation' stamp: 'MarianoMontone 6/13/2017 23:26:26'!
navigateForward
	model navigateForward! !

!BrowserWindowNav methodsFor: 'browsing' stamp: 'MarianoMontone 6/14/2017 09:38:59'!
printBrowsingHistoryItem: bhItem

	^ bhItem first printString, '>>', bhItem second printString! !

!BrowserWindowNav methodsFor: 'browsing' stamp: 'MarianoMontone 6/14/2017 19:35:30'!
showBrowsingAccess
	|menu choice|
	
	menu _ PopUpMenu labelArray: #('Most accessed' 'Recently updated').
	choice _ menu startUpMenu.
	
	choice = 1 ifTrue: [self showMostAccessed].
	choice = 2 ifTrue: [self showRecentlyUpdated]! !

!BrowserWindowNav methodsFor: 'browsing' stamp: 'MarianoMontone 6/14/2017 10:37:53'!
showBrowsingHistory
	
	| item menu |
	
	menu := SelectionMenu labelList: (self browsingHistory collect:[:x | self printBrowsingHistoryItem: x]) selections: self browsingHistory .
	item := menu startUpMenu.
	
	item ifNotNil: [
		self model browseTo: item ]! !

!BrowserWindowNav methodsFor: 'browsing' stamp: 'MarianoMontone 6/14/2017 19:27:01'!
showMostAccessed
	
	| item menu |
	
	menu := SelectionMenu labelList: (self browsingAccess collect:[:x | self printBrowsingHistoryItem: x]) selections: self browsingHistory .
	item := menu startUpMenu.
	
	item ifNotNil: [
		self model browseTo: item ]! !

!BrowserWindowNav methodsFor: 'browsing' stamp: 'MarianoMontone 6/14/2017 19:28:55'!
showRecentlyUpdated
	
	| item menu |
	
	menu := SelectionMenu labelList: (self browsingUpdates collect:[:x | self printBrowsingHistoryItem: x]) selections: self browsingHistory .
	item := menu startUpMenu.
	
	item ifNotNil: [
		self model browseTo: item ]! !

!BrowserWindowNav class methodsFor: 'as yet unclassified' stamp: 'MarianoMontone 6/14/2017 09:46:23'!
fullOnClass: aClass selector: aSelector
	"Open a new full browser set to class."

	| browser |
	browser _ BrowserNav new.
	browser setClass: aClass selector: aSelector.
	self open: browser label: browser labelString! !

!BrowserWindowNav class methodsFor: 'as yet unclassified' stamp: 'MarianoMontone 6/14/2017 09:46:19'!
openBrowser
	| browser |
	browser _ BrowserNav new.
	^ self open: browser label: browser defaultBrowserTitle! !

!BrowserWindowNav class methodsFor: 'as yet unclassified' stamp: 'MarianoMontone 6/14/2017 11:39:19'!
worldMenuForOpenGroup
	^ Dictionary new
		
			at: #itemGroup
			put: 10;
		
			at: #itemOrder
			put: 30;
		
			at: #label
			put: 'Browser Nav';
		
			at: #object
			put: self;
		
			at: #selector
			put: #openBrowser;
		
			at: #balloonText
			put: 'A Smalltalk code browser with navigation extensions';
		yourself.! !
